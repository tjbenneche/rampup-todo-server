// TodoList Server

// Require dependencies
var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
var request = require('request')

// Instantiate app
var app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

var incompleteTodos = [{name: "todo1"}, {name: "todo2"}]
var completeTodos = []

// Routes
// app.get('path', function)

app.get('/incomplete-todos', function(req, res) {
  console.log('GET incomplete todos')
  var payload = {
    data: incompleteTodos
  }
  res.json(JSON.stringify(payload))
})

app.get('/complete-todos', function(req, res) {
  console.log('GET todos')
  res.json({"test": "todos page"})
})

//app.post('/todos', function(req,res){})
//app.delete('/todos', function(req,res){})


// Run the app
app.listen(1234)
console.log('Starting up on port: 1234')
